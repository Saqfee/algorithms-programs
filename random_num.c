//program to check repeated in an array//

#include<stdio.h>
#include<stdlib.h>
#define MAX 20

int repeat_elements(int ar[], int num){
  int i,j;
  for(i = 0; i <= num; i++){
    while(1){
      i = rand() % (num+1);
      j = rand() % (num+1);
      if((i!=j) && (ar[i]=ar[j])){
	return ar[i];
      }
    }
  }   
}
  int main()
  {
    int i, ar[MAX], num;
    printf("enter the size of array\n");
    scanf("%d", &num);

    printf("enter the elements in array\n");
    for(i = 0; i < num; i++){
      scanf("%d",&ar[i]);
    }
    printf("the repeated element is %d\t", repeat_elements(ar, num));
    return 0;
  }
  

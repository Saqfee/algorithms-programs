//program to check wether a number is prime or not//

#include<stdio.h>
#include<stdlib.h>

int prime(int n){
  int a, q, m, y, z, flag;
  q = (n-1);
  for(int i = 0; i < n; i++){
    m = q;
    y = 1;
    a = rand() % (q+1);
    z = a;
    while(m > 0){
      while((m % 2) == 0){
	z = (z*z) % n;
	m = (m / 2);
      }
      m = m - 1;
      y = (y * z) % n;
    }
    if(y != 1)
      flag = 0;
    else
      flag = 1;
  }
  return flag;
}
int main(){
  int n;
  printf("Enter the number you want to check\t");
  scanf("%d", &n);
  if(prime(n) == 1)
    printf("Number is Prime\n");
    else
      printf("Number is not prime\n");
}
  
    

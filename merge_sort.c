#include<stdio.h>
#include<stdlib.h>
#define MAX 20

void merge(int a[], int l, int mid, int h){
  int i, j, k;
  int n1 =  mid - (l + 1);
  int n2 = h - mid;
  int low[n1], high[n2];

  for(i =0; i < n1; i++){
    low[i] = a[l + i];
  }
  for(j = 0; j < n2; j++){
    high[j] = a[(mid +1) + j];
  }
  i = 0;
  j = 0;
  k = 1;
    while(i < n1 && j < n2){
      if(low[i] <= high[j]){
	a[k] = low[i];
	i++;
      }
      else
	{
	  a[k] = high[j];
	  j++;
	}
      k++;
    }
}
int merge_sort(int a[], int low, int high){
  int mid;
  if(low < high){
    mid = (low + (high - 1))/2;

    merge_sort(a, low, mid);
    merge_sort(a, mid + 1, high);

    merge(a, low, mid, high);
  }
}
void print_array(int ar[], int size){
  int i;
  for(i = 0; i < size; i++)
    printf("%d", ar[i]);
  printf("\n");
}

int main(){
  int a[MAX], size;
  
  printf("enter the size of array\t");
  scanf("%d", &size);

  printf("enter the elements to be inserted in array\t\n");
  for(int i = 0; i < size; i++){
     scanf("%d\t", &a[i]);  
  }
  
  printf("Given array is:\t");
  print_array(a, size);
    
  merge_sort(a, 0, size-1);
  printf("the sorted array is:\t");
  print_array(a, size);
}
